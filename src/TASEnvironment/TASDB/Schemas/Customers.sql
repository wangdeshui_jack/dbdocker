﻿CREATE TABLE [dbo].[Customers]
(
	[Id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [CustomerName] VARCHAR(50) NOT NULL
)
