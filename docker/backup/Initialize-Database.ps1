param(
    [Parameter(Mandatory=$true)]
    [string] $file_name
)

sqlcmd -Q "RESTORE DATABASE [ARCTEMS] FROM DISK='C:\$file_name.BAK' `
                        WITH MOVE 'ARCTEMS_Data' TO 'C:\ARCTEMS.mdf',
                        MOVE 'ARCTEMS_Log' TO 'C:\ARCTEMS_log.ldf',
                        RECOVERY"